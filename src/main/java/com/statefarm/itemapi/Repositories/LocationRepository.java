package com.statefarm.itemapi.Repositories;

import com.statefarm.itemapi.Entities.Location;
import com.statefarm.itemapi.Entities.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, String> {

    Optional<Location> findByName(String name);
}
