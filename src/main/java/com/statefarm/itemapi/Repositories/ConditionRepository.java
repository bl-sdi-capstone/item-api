package com.statefarm.itemapi.Repositories;

import com.statefarm.itemapi.Entities.Condition;
import com.statefarm.itemapi.Entities.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConditionRepository extends JpaRepository<Condition, String> {

    Optional<Condition> findByName(String name);
}
