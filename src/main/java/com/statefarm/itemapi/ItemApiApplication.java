package com.statefarm.itemapi;

import com.statefarm.itemapi.Data.Seeder;
import com.statefarm.itemapi.Security.Config.JwtProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Configuration
@EnableSwagger2
public class ItemApiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ItemApiApplication.class, args);
	}

	@Bean
	public Docket productApi(){
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.statefarm.itemapi")).build();
	}

	private final Seeder seeder;

	@Autowired
	public ItemApiApplication(Seeder seeder){
		this.seeder = seeder;
	}

	@Override
	public void run(String... args) throws Exception {
		seeder.seed();
	}

	@Bean
	public JwtProperties getJwtProperties(){
		return new JwtProperties();
	}

}
