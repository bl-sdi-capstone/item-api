package com.statefarm.itemapi.Entities;

import javax.persistence.*;

@Entity
@Table(name = "condition")
public class Condition {
    @Id
    private String name;

    public Condition() {
    }

    public Condition(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
