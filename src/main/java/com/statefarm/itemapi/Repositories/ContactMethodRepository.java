package com.statefarm.itemapi.Repositories;

import com.statefarm.itemapi.Entities.ContactMethod;
import com.statefarm.itemapi.Entities.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContactMethodRepository extends JpaRepository<ContactMethod, String> {

    Optional<ContactMethod> findByName(String name);
}
