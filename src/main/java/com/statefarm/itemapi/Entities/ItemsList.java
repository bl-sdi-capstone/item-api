package com.statefarm.itemapi.Entities;

import com.statefarm.itemapi.Entities.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemsList {

    private List<Item> itemList;

    public ItemsList() {
        this.itemList = new ArrayList<>();
    }

    public ItemsList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public boolean isEmpty() {
        return this.itemList.isEmpty();
    }

    public Integer getLength() {
        return itemList.size();
    }
}
