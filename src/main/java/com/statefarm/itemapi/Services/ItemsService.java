package com.statefarm.itemapi.Services;

import com.statefarm.itemapi.Entities.*;
import com.statefarm.itemapi.Exceptions.InvalidItemException;
import com.statefarm.itemapi.Exceptions.ItemNotFoundException;
import com.statefarm.itemapi.Exceptions.UserNotAuthorizedException;
import com.statefarm.itemapi.Repositories.*;
import com.statefarm.itemapi.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class ItemsService {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ConditionRepository conditionRepository;
    @Autowired
    ContactMethodRepository contactMethodRepository;
    @Autowired
    LocationRepository locationRepository;
    @Autowired
    PaymentMethodRepository paymentMethodRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    ImageDbRepository imageDbRepository;

    ItemRepository itemRepository;

    LocalDate checkDate = LocalDate.now();

    public ItemsService(ItemRepository itemRepository, ImageDbRepository imageDbRepository) {
        this.itemRepository = itemRepository;
        this.imageDbRepository = imageDbRepository;
    }

    public ItemsList getAllItems() {
        if (checkExpirationDate()) {
            List<Item> currentList = itemRepository.findByStatusEqualsOrderByPostedDateDescTitleAsc("Active");
            for (Item item : currentList) {
                checkActiveStatus(item);
            }
            checkDate = LocalDate.now().plusDays(1);
        }
        return new ItemsList(itemRepository.findByStatusEqualsOrderByPostedDateDescTitleAsc("Active"));
    }

    public Boolean checkExpirationDate() {
        return LocalDate.now().equals(checkDate);
    }

    public void checkActiveStatus(Item item) {
        if (item.getExpirationDate().equals(checkDate)) {
            item.setStatus("Expired");
            itemRepository.save(item);
        }
    }

    public Item getItemById(Long id) {
        Optional<Item> optionalItem = itemRepository.findById(id);
        if (optionalItem.isPresent() && !optionalItem.get().getStatus().equals("Expired")) {
            return optionalItem.get();
        } else {
            throw new ItemNotFoundException();
        }
    }

    public ItemsList getActiveItemsByUsername(String username) {
        return new ItemsList(itemRepository.findByStatusEqualsAndUsernameEqualsOrderByPostedDateDescTitleAsc("Active", username));
    }

    public ItemsList getInactiveItemsByUsername(String username) {
        return new ItemsList(itemRepository.findByStatusEqualsAndUsernameEqualsOrderByPostedDateDescTitleAsc("Inactive", username));
    }

    public Item addItem(UpdateItem updateItem, User user) {
        Item item = new Item();
        boolean isValidRequest = true;

        if (user.getUsername() != null) {
            item.setUsername(user.getUsername());

            if (updateItem.getTitle() != null && updateItem.getTitle().length() != 0) {
                item.setTitle(updateItem.getTitle());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getCategory() != null && updateItem.getCategory().length() != 0) {
                item.setCategory(updateItem.getCategory());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getPrice() != null) {
                item.setPrice(updateItem.getPrice());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getContactMethod() != null && updateItem.getContactMethod().length() != 0) {
                item.setContactMethod(updateItem.getContactMethod());
            } else {
                isValidRequest = false;
            }
            if (updateItem.isWillingToShip() != null) {
                item.setWillingToShip(updateItem.isWillingToShip());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getPaymentMethod() != null && updateItem.getPaymentMethod().length() != 0) {
                item.setPaymentMethod(updateItem.getPaymentMethod());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getCondition() != null && updateItem.getCondition().length() != 0) {
                item.setCondition(updateItem.getCondition());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getDescription() != null && updateItem.getDescription().length() != 0) {
                item.setDescription(updateItem.getDescription());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getEmailAddress() != null && updateItem.getEmailAddress().length() != 0 && patternMatches(updateItem.getEmailAddress())) {
                item.setEmailAddress(updateItem.getEmailAddress());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getPhoneNumber() != null && updateItem.getPhoneNumber().length() != 0 && isValidPhoneNumber(updateItem.getPhoneNumber())) {
                item.setPhoneNumber(updateItem.getPhoneNumber());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getLocation() != null && updateItem.getLocation().length() != 0) {
                item.setLocation(updateItem.getLocation());
            } else {
                isValidRequest = false;
            }
            if (updateItem.getStatus() != null && updateItem.getStatus().equals("Active")) {
                item.setExpirationDate(LocalDate.now().plusDays(14));
                item.setStatus(updateItem.getStatus());
            } else if(updateItem.getStatus() != null && updateItem.getStatus().equals("Inactive")) {
                item.setExpirationDate(LocalDate.now().plusDays(30));
                item.setStatus(updateItem.getStatus());
            }
            else {
                isValidRequest = false;
            }
            if (updateItem.getPicture() != null) {
                item.setPicture(updateItem.getPicture());
            }
        } else {
            throw new UserNotAuthorizedException();
        }

        if (!isValidRequest) {
            throw new InvalidItemException();
        }
        return itemRepository.save(item);

    }


    public Item updateItem(Long id, UpdateItem updateItem, User user) {
        Optional<Item> foundItem = itemRepository.findById(id);
        if (foundItem.isEmpty()) {
            throw new ItemNotFoundException();
        }
        boolean isValidRequest = true;
        Item item = foundItem.get();

        if (user.getUsername().equals(updateItem.getUsername())) {

            if (updateItem.getTitle() != null) {
                item.setTitle(updateItem.getTitle());
            }
            if (updateItem.getCategory() != null) {
                item.setCategory(updateItem.getCategory());
            }
            if (updateItem.getPrice() != null) {
                item.setPrice(updateItem.getPrice());
            }
            if (updateItem.getContactMethod() != null) {
                item.setContactMethod(updateItem.getContactMethod());
            }
            if (updateItem.isWillingToShip() != null) {
                item.setWillingToShip(updateItem.isWillingToShip());
            }
            if (updateItem.getPaymentMethod() != null) {
                item.setPaymentMethod(updateItem.getPaymentMethod());
            }
            if (updateItem.getCondition() != null) {
                item.setCondition(updateItem.getCondition());
            }
            if (updateItem.getPicture() != null) {
                item.setPicture(updateItem.getPicture());
            }
            if (updateItem.getDescription() != null) {
                item.setDescription(updateItem.getDescription());
            }
            if (updateItem.getEmailAddress() != null && patternMatches(updateItem.getEmailAddress())) {
                item.setEmailAddress(updateItem.getEmailAddress());
            }
            if (updateItem.getEmailAddress() != null && !patternMatches(updateItem.getEmailAddress())) {
                isValidRequest = false;
            }
            if (updateItem.getPhoneNumber() != null && isValidPhoneNumber(updateItem.getPhoneNumber())) {
                item.setPhoneNumber(updateItem.getPhoneNumber());
            }
            if (updateItem.getPhoneNumber() != null && !isValidPhoneNumber(updateItem.getPhoneNumber())) {
                isValidRequest = false;
            }
            if (updateItem.getViolationFlagged() != null) {
                item.setViolationFlagged(updateItem.getViolationFlagged());
            }
            if (updateItem.getStatus() != null && updateItem.getStatus().equals("Active")) {
                item.setExpirationDate(LocalDate.now().plusDays(14));
                item.setStatus(updateItem.getStatus());
            } else if(updateItem.getStatus() != null && updateItem.getStatus().equals("Inactive")) {
                item.setExpirationDate(LocalDate.now().plusDays(30));
                item.setStatus(updateItem.getStatus());
            }

        } else {
            throw new UserNotAuthorizedException();
        }
        if (isValidRequest) {
            return itemRepository.save(item);
        } else {
            throw new InvalidItemException();
        }
    }

    public Item updateItemByTitle(String title, UpdateItem updateItem) {
        Optional<Item> foundItem = itemRepository.findByTitle(title);
        if (foundItem.isEmpty()) {
            throw new ItemNotFoundException();
        }
        boolean isValidRequest = true;
        Item item = foundItem.get();
        if (updateItem.getTitle() != null) {
            item.setTitle(updateItem.getTitle());
        }
        if (updateItem.getCategory() != null) {
            item.setCategory(updateItem.getCategory());
        }
        if (updateItem.getPrice() != null) {
            item.setPrice(updateItem.getPrice());
        }
        if (updateItem.getContactMethod() != null) {
            item.setContactMethod(updateItem.getContactMethod());
        }
        if (updateItem.isWillingToShip() != null) {
            item.setWillingToShip(updateItem.isWillingToShip());
        }
        if (updateItem.getPaymentMethod() != null) {
            item.setPaymentMethod(updateItem.getPaymentMethod());
        }
        if (updateItem.getCondition() != null) {
            item.setCondition(updateItem.getCondition());
        }
        if (updateItem.getPicture() != null) {
            item.setPicture(updateItem.getPicture());
        }
        if (updateItem.getDescription() != null) {
            item.setDescription(updateItem.getDescription());
        }
        if (updateItem.getEmailAddress() != null && patternMatches(updateItem.getEmailAddress())) {
            item.setEmailAddress(updateItem.getEmailAddress());
        }
        if (updateItem.getEmailAddress() != null && !patternMatches(updateItem.getEmailAddress())) {
            isValidRequest = false;
        }
        if (updateItem.getPhoneNumber() != null && isValidPhoneNumber(updateItem.getPhoneNumber())) {
            item.setPhoneNumber(updateItem.getPhoneNumber());
        }
        if (updateItem.getPhoneNumber() != null && !isValidPhoneNumber(updateItem.getPhoneNumber())) {
            isValidRequest = false;
        }
        if (updateItem.getViolationFlagged() != null) {
            item.setViolationFlagged(updateItem.getViolationFlagged());
        }
        if (updateItem.getStatus() != null && (updateItem.getStatus().equals("Active") || updateItem.getStatus().equals("Inactive"))) {
            item.setStatus(updateItem.getStatus());
        }

        if (isValidRequest) {
            return itemRepository.save(item);
        } else {
            throw new InvalidItemException();
        }
    }

    public Item updateViews(Long id) {
        Optional<Item> foundItem = itemRepository.findById(id);
        if (foundItem.isEmpty()) {
            throw new ItemNotFoundException();
        }
        Item item = foundItem.get();
        item.setViews(item.getViews() + 1);
        return itemRepository.save(item);
    }

    public Item updateViolationFlagged(Long id, UpdateItem updateItem) {
        Optional<Item> foundItem = itemRepository.findById(id);
        if (foundItem.isEmpty()) {
            throw new ItemNotFoundException();
        }

        Item item = foundItem.get();
        item.setViolationFlagged(updateItem.getViolationFlagged());
        return itemRepository.save(item);
    }

    public void deleteItem(long id, User user) {
        Optional<Item> optionalItem = itemRepository.findById(id);
        if (optionalItem.isPresent()) {
            if (user.getUsername().equals(optionalItem.get().getUsername())) {
                itemRepository.delete(optionalItem.get());
            } else {
                throw new UserNotAuthorizedException();
            }
        } else {
            throw new ItemNotFoundException();
        }
    }

    public SeederDropdowns getSeeder() {
        SeederDropdowns seederDropdowns = new SeederDropdowns();
        seederDropdowns.setCategory(categoryRepository.findAll());
        seederDropdowns.setCondition(conditionRepository.findAll());
        seederDropdowns.setContactMethod(contactMethodRepository.findAll());
        seederDropdowns.setLocation(locationRepository.findAll());
        seederDropdowns.setPaymentMethod(paymentMethodRepository.findAll());
        seederDropdowns.setStatus(statusRepository.findAll());
        return seederDropdowns;
    }

    public boolean patternMatches(String emailAddress) {
        String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        return Pattern.compile(regexPattern)
                .matcher(emailAddress)
                .matches();
    }

    public boolean isValidPhoneNumber(String phoneNumber) {
        String pattern = "^((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";
        return Pattern.compile(pattern)
                .matcher(phoneNumber)
                .matches();
    }
}
