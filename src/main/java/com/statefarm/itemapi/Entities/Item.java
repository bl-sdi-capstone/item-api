package com.statefarm.itemapi.Entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String category;
    private int price;
    private String location;
    private String contactMethod;
    private boolean willingToShip;
    private LocalDate expirationDate = LocalDate.now().plusDays(14);
    private LocalDate postedDate = LocalDate.now();
    private boolean violationFlagged = false;
    private String paymentMethod;
    private String phoneNumber;
    private String emailAddress;
    private String condition;
    private Long picture;
    private String description;
    private int views = 0;
    private String status;
    private String username;

    public Item(String title, String category, int price, String location, String contactMethod, boolean willingToShip, String paymentMethod, String phoneNumber, String emailAddress, String condition, Long picture, String description, String status) {
        this.title = title;
        this.category = category;
        this.price = price;
        this.location = location;
        this.contactMethod = contactMethod;
        this.willingToShip = willingToShip;
        this.paymentMethod = paymentMethod;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.condition = condition;
        this.picture = picture;
        this.description = description;
        this.status = status;
        postedDate = LocalDate.now();
        expirationDate = this.postedDate.plusDays(14);
    }

    public Item() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(String contactMethod) {
        this.contactMethod = contactMethod;
    }

    public boolean isWillingToShip() {
        return willingToShip;
    }

    public void setWillingToShip(boolean willingToShip) {
        this.willingToShip = willingToShip;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public LocalDate getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(LocalDate postedDate) {
        this.postedDate = postedDate;
    }

    public boolean isViolationFlagged() {
        return violationFlagged;
    }

    public void setViolationFlagged(boolean violationFlagged) {
        this.violationFlagged = violationFlagged;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Long getPicture() {
        return picture;
    }

    public void setPicture(Long picture) {
        this.picture = picture;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
