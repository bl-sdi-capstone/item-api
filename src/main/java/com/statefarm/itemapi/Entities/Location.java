package com.statefarm.itemapi.Entities;

import javax.persistence.*;

@Entity
@Table(name = "location")
public class Location {

    @Id
    private String name;

    public Location() {
    }

    public Location(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
