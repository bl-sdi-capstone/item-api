package com.statefarm.itemapi.Entities;

import java.util.List;

public class SeederDropdowns {

    private List<Category> category;
    private List<Condition> condition;
    private List<ContactMethod> contactMethod;
    private List<Location> location;
    private List<PaymentMethod> paymentMethod;
    private List<Status> status;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public List<Condition> getCondition() {
        return condition;
    }

    public void setCondition(List<Condition> condition) {
        this.condition = condition;
    }

    public List<ContactMethod> getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(List<ContactMethod> contactMethod) {
        this.contactMethod = contactMethod;
    }

    public List<Location> getLocation() {
        return location;
    }

    public void setLocation(List<Location> location) {
        this.location = location;
    }

    public List<PaymentMethod> getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(List<PaymentMethod> paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<Status> getStatus() {
        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }
}
