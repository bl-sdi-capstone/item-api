package com.statefarm.itemapi.Repositories;

import com.statefarm.itemapi.Entities.Category;
import com.statefarm.itemapi.Entities.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, String> {
    Optional<Category> findByName(String name);
}
