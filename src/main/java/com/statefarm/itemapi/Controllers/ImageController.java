package com.statefarm.itemapi.Controllers;

import com.statefarm.itemapi.Entities.Image;
import com.statefarm.itemapi.Entities.Item;
import com.statefarm.itemapi.Exceptions.ItemNotFoundException;
import com.statefarm.itemapi.Exceptions.UserNotAuthorizedException;
import com.statefarm.itemapi.Model.User;
import com.statefarm.itemapi.Repositories.ImageDbRepository;
import com.statefarm.itemapi.Repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
public class ImageController {
    @Autowired
    ImageDbRepository imageDbRepository;
    @Autowired
    ItemRepository itemRepository;

    Long imageId;

    @PostMapping("/api/items/{id}/image")
    Item uploadImage(@RequestParam(required = false) MultipartFile multipartImage, @PathVariable Long id, @AuthenticationPrincipal User user) throws Exception {
        if (user.getUsername() != null) {
            Optional<Item> item = itemRepository.findById(id);
            if (multipartImage != null) {
                Image dbImage = new Image();
                dbImage.setName(multipartImage.getName());
                dbImage.setContent(multipartImage.getBytes());
                imageId = imageDbRepository.save(dbImage).getId();
            } else {
                Optional<Long> optionalDefaultImageId = imageDbRepository.findByName("DefaultImage");
                if (optionalDefaultImageId.isPresent()) {
                    if (item.isPresent()) {
                        imageId = optionalDefaultImageId.get();
                    }
                }
            }
            if (item.isPresent()) {
                Long currentImageId = item.get().getPicture();
                Optional<Image> currentImage = imageDbRepository.findById(currentImageId);
                if (currentImage.isPresent() && !currentImage.get().getName().equals("DefaultImage")) {
                    imageDbRepository.deleteById(currentImageId);
                }
                item.get().setPicture(imageId);
                itemRepository.save(item.get());
                return item.get();
            } else {
                throw new ItemNotFoundException();
            }
        } else {
            throw new UserNotAuthorizedException();
        }
    }

    @GetMapping(value = "/api/images/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    ByteArrayResource getImageByImageId(@PathVariable Long id) {
        byte[] image = imageDbRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                .getContent();

        return new ByteArrayResource(image);
    }

    @GetMapping(value = "/api/items/{id}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    ByteArrayResource getImageByItemId(@PathVariable Long id) {
        Optional<Item> item = itemRepository.findById(id);
        if (item.isPresent()) {
            Long imageId = item.get().getPicture();
            byte[] image = imageDbRepository.findById(imageId)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                    .getContent();

            return new ByteArrayResource(image);
        } else {
            throw new ItemNotFoundException();
        }
    }
}
