package com.statefarm.itemapi.Entities;

import javax.persistence.*;

@Entity
@Table(name = "status")
public class Status {

    @Id
    private String name;

    public Status() {
    }

    public Status(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
