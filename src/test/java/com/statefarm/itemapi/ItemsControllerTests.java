package com.statefarm.itemapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.statefarm.itemapi.Controllers.ItemsController;
import com.statefarm.itemapi.Data.Seeder;
import com.statefarm.itemapi.Entities.*;
import com.statefarm.itemapi.Exceptions.InvalidItemException;
import com.statefarm.itemapi.Exceptions.ItemNotFoundException;
import com.statefarm.itemapi.Exceptions.UserNotAuthorizedException;
import com.statefarm.itemapi.Services.ItemsService;
import com.statefarm.itemapi.Model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ItemsController.class)
public class ItemsControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ItemsService itemService;

    @MockBean
    Seeder seeder;

    ObjectMapper objectMapper = new ObjectMapper();
    List<Item> itemList = new ArrayList<Item>();
    List<Item> inactiveItemList = new ArrayList<>();
    SeederDropdowns seederDropdowns = new SeederDropdowns();

    TokenTestUtils tokenTestUtils = new TokenTestUtils("blah");
    String token = tokenTestUtils.getToken("userA", Arrays.asList("ROLE_USER"));
    String adminToken = tokenTestUtils.getToken("userB", Arrays.asList("ROLE_ADMIN"));

    @BeforeEach
    void setUp() {

        for (int i = 0; i < 5; i++) {
            Item item = new Item();
            item.setUsername("UserA");
            itemList.add(item);
        }

        for (int i = 0; i < 5; i++) {
            Item item = new Item();
            item.setUsername("UserB");
            inactiveItemList.add(item);
        }

        List<Category> categoryList = new ArrayList<>();
        categoryList.add(new Category("Houses"));
        categoryList.add(new Category("Tools"));
        categoryList.add(new Category("Cars"));
        categoryList.add(new Category("Land"));
        categoryList.add(new Category("Electronics"));
        categoryList.add(new Category("Movies"));

        List<Condition> conditionList = new ArrayList<>();
        conditionList.add(new Condition("Good"));
        conditionList.add(new Condition("Acceptable"));
        conditionList.add(new Condition("Like New"));

        List<ContactMethod> contactMethodList = new ArrayList<>();
        contactMethodList.add(new ContactMethod("Phone Call"));
        contactMethodList.add(new ContactMethod("Text Message"));
        contactMethodList.add(new ContactMethod("Email"));

        List<Location> locationList = new ArrayList<>();
        locationList.add(new Location("Bloomington"));
        locationList.add(new Location("Dallas"));
        locationList.add(new Location("Phoenix"));
        locationList.add(new Location("Atlanta"));

        List<PaymentMethod> paymentMethodList = new ArrayList<>();
        paymentMethodList.add(new PaymentMethod("Cash"));
        paymentMethodList.add(new PaymentMethod("Paypal"));
        paymentMethodList.add(new PaymentMethod("Venmo"));
        paymentMethodList.add(new PaymentMethod("Check"));
        paymentMethodList.add(new PaymentMethod("Gold Nuggets"));

        List<Status> statusList = new ArrayList<>();
        statusList.add(new Status("New"));
        statusList.add(new Status("Pending"));
        statusList.add(new Status("Sold"));

        seederDropdowns.setCondition(conditionList);
        seederDropdowns.setCategory(categoryList);
        seederDropdowns.setContactMethod(contactMethodList);
        seederDropdowns.setLocation(locationList);
        seederDropdowns.setPaymentMethod(paymentMethodList);
        seederDropdowns.setStatus(statusList);

    }

    @Test
    void getAllItems() throws Exception {
        when(itemService.getAllItems()).thenReturn(new ItemsList(itemList));
        mockMvc.perform(get("/api/items"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.itemList", hasSize(5)));
    }

    @Test
    void getAllItemsReturnsNothing() throws Exception {
        when(itemService.getAllItems()).thenReturn(new ItemsList());
        mockMvc.perform(get("/api/items"))
                .andExpect(status().isNoContent());
    }

    @Test
    void getItemById() throws Exception {
        Item item = itemList.get(0);
        item.setId(1L);
        when(itemService.getItemById(anyLong())).thenReturn(item);
        mockMvc.perform(get("/api/items/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(item.getId()));
    }

    @Test
    void getItemByIdReturnsNothing() throws Exception {
        doThrow(new ItemNotFoundException()).when(itemService).getItemById(anyLong());
        mockMvc.perform(get("/api/items/12345"))
                .andExpect(status().isNoContent());
    }

    @Test
    void getActiveItemsByUsernameReturnsList() throws Exception {
        when(itemService.getActiveItemsByUsername(anyString())).thenReturn(new ItemsList(itemList));
        mockMvc.perform(get("/api/items/active/UserA"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.itemList", hasSize(5)));
    }

    @Test
    void getInactiveItemsByUsernameReturnsList() throws Exception {
        when(itemService.getInactiveItemsByUsername(anyString())).thenReturn(new ItemsList(inactiveItemList));
        mockMvc.perform(get("/api/items/inactive/UserB"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.itemList", hasSize(5)));
    }

    @Test
    void addItem() throws Exception {
        Item item = itemList.get(0);
        item.setId(1L);

        when(itemService.addItem(any(UpdateItem.class), any(User.class))).thenReturn(item);

        mockMvc.perform(post("/api/items")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(item.getId()));
    }

    @Test
    void addItemBadRequest() throws Exception {
        Item item = new Item();
        item.setTitle("Blah");
        when(itemService.addItem(any(UpdateItem.class), any(User.class))).thenThrow(InvalidItemException.class);
        mockMvc.perform(post("/api/items")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\", \"title\":\"Blah\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateItem() throws Exception {
        Item item = itemList.get(0);
        item.setTitle("lawn mower");
        item.setId(1L);
        Item updatedItem = new Item();
        updatedItem.setId(item.getId());
        updatedItem.setTitle("vacuum");
        when(itemService.updateItem(eq(item.getId()), any(UpdateItem.class), any(User.class))).thenReturn(updatedItem);
        mockMvc.perform(patch("/api/items/" + item.getId())
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\":\"vacuum\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title").value("vacuum"));
    }

    @Test
    void updateItemNoItemFound() throws Exception {
        Item item = new Item();
        doThrow(new ItemNotFoundException()).when(itemService).updateItem(anyLong(), any(UpdateItem.class), any(User.class));
        mockMvc.perform(patch("/api/items/12345")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\"}"))
                .andExpect(status().isNoContent());
    }

    @Test
    void updateItemBadRequest() throws Exception {
        Item item = new Item();
        doThrow(new InvalidItemException()).when(itemService).updateItem(anyLong(), any(UpdateItem.class), any(User.class));
        mockMvc.perform(patch("/api/items/12345")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateViewCount() throws Exception {
        Item item = itemList.get(0);
        item.setViews(1);
        item.setId(1L);
        UpdateItem updateItem = new UpdateItem();
        updateItem.setViews(1);
        when(itemService.updateViews(anyLong())).thenReturn(item);
        mockMvc.perform(patch("/api/items/" + item.getId() + "/views")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateItem)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("views").value(1));
    }

    @Test
    void updateViolationFlagged() throws Exception {
        Item item = itemList.get(0);
        item.setViolationFlagged(true);
        item.setId(1L);
        UpdateItem updateItem = new UpdateItem();
        updateItem.setViolationFlagged(true);
        when(itemService.updateViolationFlagged(anyLong(), any(UpdateItem.class))).thenReturn(item);
        mockMvc.perform(patch("/api/items/" + item.getId() + "/report")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateItem)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("violationFlagged").value(true));

    }

    @Test
    void deleteItemById() throws Exception {
        mockMvc.perform(delete("/api/items/1")
                        .header("Authorization", token))
                .andExpect(status().isAccepted());
        verify(itemService).deleteItem(anyLong(), any(User.class));
    }

    @Test
    void deleteItemByIdNoItemFound() throws Exception {
        doThrow(new ItemNotFoundException()).when(itemService).deleteItem(anyLong(), any(User.class));
        mockMvc.perform(delete("/api/items/12345")
                        .header("Authorization", token))
                .andExpect(status().isNoContent());
    }

    @Test
    void getAllSeederDropDownData() throws Exception {
        when(itemService.getSeeder()).thenReturn(seederDropdowns);
        mockMvc.perform(get("/api/seeder"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.category", hasSize(6)));
    }

    @Test
    void addItemUnauthorized() throws Exception {
        UpdateItem updateItem = new UpdateItem();
        updateItem.setTitle("Cool item");

        when(itemService.addItem(any(UpdateItem.class), any(User.class))).thenThrow(UserNotAuthorizedException.class);

        mockMvc.perform(post("/api/items")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateItem)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void updateItemUnauthorized() throws Exception {
        Item item = new Item();
        item.setId(1L);

        UpdateItem updateItem = new UpdateItem();
        updateItem.setTitle("Cool item");

        when(itemService.updateItem(anyLong(), any(UpdateItem.class), any(User.class))).thenThrow(UserNotAuthorizedException.class);

        mockMvc.perform(patch("/api/items/" + item.getId())
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateItem)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void deleteItemUnauthorized() throws Exception {
        Item item = new Item();
        item.setId(1L);

        doThrow(new UserNotAuthorizedException()).when(itemService).deleteItem(anyLong(), any(User.class));

        mockMvc.perform(delete("/api/items/" + item.getId())
                        .header("Authorization", token))
                .andExpect(status().isUnauthorized());
    }
}
