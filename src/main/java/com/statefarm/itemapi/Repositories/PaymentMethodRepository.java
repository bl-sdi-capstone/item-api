package com.statefarm.itemapi.Repositories;

import com.statefarm.itemapi.Entities.PaymentMethod;
import com.statefarm.itemapi.Entities.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, String> {

    Optional<PaymentMethod> findByName(String name);
}
