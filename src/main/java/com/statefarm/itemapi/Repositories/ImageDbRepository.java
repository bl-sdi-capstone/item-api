package com.statefarm.itemapi.Repositories;

import com.statefarm.itemapi.Entities.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageDbRepository  extends JpaRepository<Image, Long> {

    @Query(value = "SELECT i.id FROM image i WHERE i.name = ?1", nativeQuery = true)
    Optional<Long> findByName(String name);
}
