package com.statefarm.itemapi;

import com.statefarm.itemapi.Entities.Image;
import com.statefarm.itemapi.Entities.Item;
import com.statefarm.itemapi.Entities.ItemsList;
import com.statefarm.itemapi.Repositories.ImageDbRepository;
import com.statefarm.itemapi.Repositories.ItemRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ItemApiApplicationTests {
	Item item;

	@Autowired
	TestRestTemplate restTemplate;

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	ImageDbRepository imageDbRepository;

	String username = "UserA";
	String generateToken() {
		TokenTestUtils tokenTestUtils = new TokenTestUtils("blah");
		return tokenTestUtils.getToken(username, List.of("ROLE_USER"));
	}

	String invalidUsername = "UserB";
	String invalidToken() {
		TokenTestUtils tokenTestUtils = new TokenTestUtils("wrongblah");
		return tokenTestUtils.getToken(invalidUsername, List.of("ROLE_USER"));
	}

	@BeforeEach
	void setUp() {

		item = new Item("Super awesome item!",
				"Household",
				100,
				"Bloomington",
				"Phone",
				true,
				"Venmo",
				"555-555-5555",
				"Goodseller@alfi.com",
				"good",
				1L,
				"the best household item ever. Even better than a chair.",
				"Active");
		item.setUsername("UserA");

		Item inactiveItem = new Item("Super awesome item!",
				"Household",
				100,
				"Bloomington",
				"Phone",
				true,
				"Venmo",
				"555-555-5555",
				"Goodseller@alfi.com",
				"good",
				1L,
				"the best household item ever. Even better than a chair.",
				"Inactive");
		inactiveItem.setUsername("UserB");

		itemRepository.save(item);
		itemRepository.save(inactiveItem);
	}

	@AfterEach
	void tearDown() {
		itemRepository.deleteAll();
	}

	@Test
	void contextLoads() {
	}

	@Test
	void getAllItems() {
		ResponseEntity<ItemsList> response = restTemplate.getForEntity("/api/items", ItemsList.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isNotNull();
	}

	@Test
	void getItemById() {
		ResponseEntity<Item> response = restTemplate.getForEntity("/api/items/" + item.getId(), Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isNotNull();
		assertThat(response.getBody().getId()).isEqualTo(item.getId());
	}

	@Test
	void getActiveItemsByUsernameReturnsList() {
		ResponseEntity<ItemsList> response = restTemplate.getForEntity("/api/items/active/UserA", ItemsList.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isNotNull();
	}

	@Test
	void getInactiveItemsByUsernameReturnsList() {
		ResponseEntity<ItemsList> response = restTemplate.getForEntity("/api/items/inactive/UserB", ItemsList.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isNotNull();
	}

	@Test
	void addItem() {
		Item newItem = new Item();
		newItem.setTitle("New Item");
		newItem.setCategory("Furniture");
		newItem.setPrice(10);
		newItem.setContactMethod("Call");
		newItem.setWillingToShip(true);
		newItem.setPaymentMethod("Venmo");
		newItem.setCondition("Good");
		newItem.setDescription("New item in great condition");
		newItem.setEmailAddress("email@email.com");
		newItem.setPhoneNumber("123-456-7890");
		newItem.setLocation("Bloomington");
		newItem.setUsername("UserA");
		newItem.setStatus("Active");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Authorization", generateToken());
		HttpEntity<Item> request = new HttpEntity<>(newItem, headers);

		ResponseEntity<Item> response = restTemplate.postForEntity("/api/items", request, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(Objects.requireNonNull(response.getBody()).getUsername()).isEqualTo(username);
		assertThat(Objects.requireNonNull(response.getBody()).getTitle()).isEqualTo("New Item");
		assertThat(Objects.requireNonNull(response.getBody()).getStatus()).isEqualTo("Active");
	}

	@Test
	void updateItemById() throws JSONException {
		String url = "/api/items/" + item.getId();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Authorization", generateToken());

		JSONObject updateBody = new JSONObject();
		updateBody.put("title", "Griswold Eggnog");
		updateBody.put("username", "UserA");
		updateBody.put("status", "Inactive");
		String updateBodyString = updateBody.toString();

		HttpEntity<String> request = new HttpEntity<>(updateBodyString, headers);
		HttpEntity<Item> response = restTemplate.exchange(url, HttpMethod.PATCH, request, Item.class);

		assertThat(response).isNotNull();
		assertThat(Objects.requireNonNull(response.getBody()).getTitle()).isEqualTo("Griswold Eggnog");
		assertThat(Objects.requireNonNull(response.getBody()).getStatus()).isEqualTo("Inactive");
	}

	@Test
	void updateViews() throws JSONException {
		String url = "/api/items/" + item.getId() + "/views";

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

		JSONObject updateBody = new JSONObject();
		String updateBodyString = updateBody.toString();

		HttpEntity<String> request = new HttpEntity<>(updateBodyString, headers);
		HttpEntity<Item> response = restTemplate.exchange(url, HttpMethod.PATCH, request, Item.class);

		assertThat(response).isNotNull();
		assertThat(Objects.requireNonNull(response.getBody()).getViews()).isEqualTo(1);
	}

	@Test
	void updateViolationFlagged() throws JSONException {
		String url = "/api/items/" + item.getId() + "/report";

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

		JSONObject updateBody = new JSONObject();
		updateBody.put("violationFlagged", true);
		String updateBodyString = updateBody.toString();

		HttpEntity<String> request = new HttpEntity<>(updateBodyString, headers);
		HttpEntity<Item> response = restTemplate.exchange(url, HttpMethod.PATCH, request, Item.class);

		assertThat(response).isNotNull();
		assertThat(Objects.requireNonNull(response.getBody()).isViolationFlagged()).isEqualTo(true);
	}

	@Test
	void deleteItemById() {
		String url = "/api/items/" + item.getId();

		Optional<Item> itemToDelete = itemRepository.findById(item.getId());
		assertThat(itemToDelete).isNotEmpty();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", generateToken());

		HttpEntity<String> request = new HttpEntity<>("", headers);

		ResponseEntity<Item> response =  restTemplate.exchange(url, HttpMethod.DELETE, request, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
	}

	@Test
	void uploadImageIntegrationTest() {
		Item imageItem = new Item();
		imageItem.setUsername("UserA");
		Image newImage = new Image();
		newImage.setName("image");
		Long imageId = imageDbRepository.save(newImage).getId();
		imageItem.setPicture(imageId);
		itemRepository.save(imageItem);

		String url = "/api/items/" + imageItem.getId() + "/image";
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();

		parameters.add("multipartImage", new org.springframework.core.io.ClassPathResource("blender.jpg"));

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
		headers.set("Authorization", generateToken());
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<LinkedMultiValueMap<String, Object>>(parameters, headers);

		ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	void callingUploadImageEndpointProvidingNoImage_SetsDefaultImage() {
		Item imageItem = new Item();
		imageItem.setUsername("UserA");
		Long imageId = imageDbRepository.findByName("DefaultImage").get();
		imageItem.setPicture(imageId);
		itemRepository.save(imageItem);

		String url = "/api/items/" + imageItem.getId() + "/image";
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();

		parameters.add("multipartImage", "");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
		headers.set("Authorization", generateToken());
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<LinkedMultiValueMap<String, Object>>(parameters, headers);

		ResponseEntity<Item> response = restTemplate.postForEntity(url, entity, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(imageDbRepository.findByName("DefaultImage").get()).isEqualTo(response.getBody().getPicture());
	}

	@Test
	void getImageByImageId() {
		Image image = new Image();
		imageDbRepository.save(image);
		String url = "/api/images/" + image.getId();

		Image returnedImage = restTemplate.getForObject(url, Image.class);

		assertThat(returnedImage).isNotNull();
		assertThat(returnedImage.getName()).isEqualTo(image.getName());
	}

	@Test
	void getImageByItemId() {
		Long itemId = item.getId();
		Item item = itemRepository.getById(itemId);
		String url = "/api/items/" + itemId + "/image";

		Image returnedImage = restTemplate.getForObject(url, Image.class);

		assertThat(returnedImage).isNotNull();
	}

	@Test
	void addItemUnauthorizedUserNoToken() {
		Item newItem = new Item();
		newItem.setTitle("New Item");
		newItem.setCategory("Furniture");
		newItem.setPrice(10);
		newItem.setContactMethod("Call");
		newItem.setWillingToShip(true);
		newItem.setPaymentMethod("Venmo");
		newItem.setCondition("Good");
		newItem.setDescription("New item in great condition");
		newItem.setEmailAddress("email@email.com");
		newItem.setPhoneNumber("123-456-7890");
		newItem.setLocation("Bloomington");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<Item> request = new HttpEntity<>(newItem, headers);

		ResponseEntity<Item> response = restTemplate.postForEntity("/api/items", request, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	void updateItemByIdNoToken() throws JSONException {
		String url = "/api/items/" + item.getId();
		item.setUsername("13451");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

		JSONObject updateBody = new JSONObject();
		updateBody.put("title", "Griswold Eggnog");

		String updateBodyString = updateBody.toString();
		HttpEntity<String> request = new HttpEntity<>(updateBodyString, headers);
		ResponseEntity<Item> response =  restTemplate.exchange(url, HttpMethod.PATCH, request, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	void updateItemUnauthorizedUserWrongUser() throws JSONException {

		TokenTestUtils tokenUtil = new TokenTestUtils("blah");
		String testToken = tokenUtil.getToken("userB", List.of("ROLE_USER"));

		String url = "/api/items/" + item.getId();
		item.setUsername("13451");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Authorization", testToken);

		JSONObject updateBody = new JSONObject();
		updateBody.put("title", "Griswold Eggnog");

		String updateBodyString = updateBody.toString();
		HttpEntity<String> request = new HttpEntity<>(updateBodyString, headers);
		ResponseEntity<Item> response =  restTemplate.exchange(url, HttpMethod.PATCH, request, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	void deleteItemNoToken() {

		String url = "/api/items/" + item.getId();

		Optional<Item> itemToDelete = itemRepository.findById(item.getId());
		assertThat(itemToDelete).isNotEmpty();

		HttpHeaders headers = new HttpHeaders();

		HttpEntity<String> request = new HttpEntity<>("", headers);

		ResponseEntity<Item> response =  restTemplate.exchange(url, HttpMethod.DELETE, request, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	void deleteItemUnauthorizedUserWrongUser() {
		TokenTestUtils tokenUtil = new TokenTestUtils("blah");
		String testToken = tokenUtil.getToken("userB", List.of("ROLE_USER"));

		String url = "/api/items/" + item.getId();

		Optional<Item> itemToDelete = itemRepository.findById(item.getId());
		assertThat(itemToDelete).isNotEmpty();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", testToken);
		HttpEntity<String> request = new HttpEntity<>("", headers);

		ResponseEntity<Item> response =  restTemplate.exchange(url, HttpMethod.DELETE, request, Item.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}

	@Test
	void postImageUnauthorized() {
		String url = "/api/items/" + item.getId() + "/image";
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
		parameters.add("multipartImage", new org.springframework.core.io.ClassPathResource("blender.jpg"));

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
		headers.set("Authorization", invalidToken());
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<LinkedMultiValueMap<String, Object>>(parameters, headers);

		ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}
}
