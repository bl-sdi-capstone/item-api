package com.statefarm.itemapi;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;

public class TokenTestUtils {

    String jwt_key;

    public TokenTestUtils(String jwt_key) {
        this.jwt_key = jwt_key;
    }

    public String getToken(String username, List<String> roles){
        long now = System.currentTimeMillis();
        String token = Jwts.builder()
                .setHeaderParam("typ","JWT")
                .setSubject(username)
                .claim("name", username)
                .claim("guid", 99)
                // Convert to list of strings.
                // This is important because it affects the way we get them back in the Gateway.
                .claim("authorities", roles)
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + 5256000 * 1000L))  // in milliseconds
                .signWith(SignatureAlgorithm.HS512, jwt_key.getBytes())
                .compact();

        return String.format("Bearer %s", token);
    }
}

