package com.statefarm.itemapi.Controllers;

import com.statefarm.itemapi.Entities.Item;
import com.statefarm.itemapi.Entities.ItemsList;
import com.statefarm.itemapi.Entities.SeederDropdowns;
import com.statefarm.itemapi.Entities.UpdateItem;
import com.statefarm.itemapi.Exceptions.InvalidItemException;
import com.statefarm.itemapi.Exceptions.ItemNotFoundException;
import com.statefarm.itemapi.Exceptions.UserNotAuthorizedException;
import com.statefarm.itemapi.Model.User;
import com.statefarm.itemapi.Services.ItemsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class ItemsController {

    ItemsService itemService;

    ItemsList itemsList;

    public ItemsController(ItemsService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("/api/items")
    public ResponseEntity<ItemsList> getAllItemsCall(){
        itemsList = itemService.getAllItems();
        if (itemsList.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(itemsList);
        }
    }

    @GetMapping("/api/items/{id}")
    public ResponseEntity<Item> getItemById(@PathVariable Long id) {
        Item item = itemService.getItemById(id);
        return item == null ? ResponseEntity.noContent().build() : ResponseEntity.ok(item);
    }

    @GetMapping("/api/seeder")
    public SeederDropdowns getSeederDropdownData() {
        return itemService.getSeeder();
    }

    @GetMapping("/api/items/active/{username}")
    public ResponseEntity<ItemsList> getActiveItemsByUsername(@PathVariable String username) {
        itemsList = itemService.getActiveItemsByUsername(username);
        if (itemsList.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(itemsList);
        }
    }

    @GetMapping("/api/items/inactive/{username}")
    public ResponseEntity<ItemsList> getInactiveItemsByUsername(@PathVariable String username) {
        itemsList = itemService.getInactiveItemsByUsername(username);
        if (itemsList.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(itemsList);
        }
    }

    @PostMapping("/api/items")
    public Item addItem(@RequestBody UpdateItem updateItem, @AuthenticationPrincipal User user) {
        return itemService.addItem(updateItem, user);
    }

    @PatchMapping("/api/items/{id}")
    public Item updateItem(@PathVariable Long id,
                           @RequestBody UpdateItem updateItem,
                           @AuthenticationPrincipal User user) throws IllegalAccessException{
       return itemService.updateItem(id, updateItem, user);
    }

    @PatchMapping("/api/items/{id}/views")
    public Item updateViews(@PathVariable Long id) {
        return itemService.updateViews(id);
    }

    @PatchMapping("/api/items/{id}/report")
    public Item updateViolationFlagged(@PathVariable Long id,
                            @RequestBody UpdateItem updateItem) {
        return itemService.updateViolationFlagged(id, updateItem);
    }

    @DeleteMapping("/api/items/{id}")
    public ResponseEntity<Void> deleteItem(@PathVariable Long id, @AuthenticationPrincipal User user){
        itemService.deleteItem(id, user);
        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void itemNotFoundExceptionHandler(ItemNotFoundException e) {

    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void invalidItemExceptionHandler(InvalidItemException e) {

    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void userNotAuthorizedExceptionHandler(UserNotAuthorizedException e) {

    }

}
