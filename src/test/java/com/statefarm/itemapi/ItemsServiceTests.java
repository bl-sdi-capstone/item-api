package com.statefarm.itemapi;

import com.statefarm.itemapi.Entities.Image;
import com.statefarm.itemapi.Entities.Item;
import com.statefarm.itemapi.Entities.ItemsList;
import com.statefarm.itemapi.Entities.UpdateItem;
import com.statefarm.itemapi.Exceptions.InvalidItemException;
import com.statefarm.itemapi.Exceptions.ItemNotFoundException;
import com.statefarm.itemapi.Exceptions.UserNotAuthorizedException;
import com.statefarm.itemapi.Repositories.ImageDbRepository;
import com.statefarm.itemapi.Repositories.ItemRepository;
import com.statefarm.itemapi.Services.ItemsService;
import com.statefarm.itemapi.Model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ItemsServiceTests {

    private ItemsService itemsService;
    List<Item> itemList = new ArrayList<Item>();
    List<Item> inactiveItemList = new ArrayList<>();

    User user = new User(7L, "blah", "blah", "also blah", "still blah", "eaeghea");

    Image image = new Image();

    @Mock
    ItemRepository itemRepository;

    @Mock
    ImageDbRepository imageDbRepository;

    @BeforeEach
    void setUp() {
        itemsService = new ItemsService(itemRepository, imageDbRepository);
        for(int i = 0; i < 5 ; i++){
            Item item = new Item();
            item.setStatus("Active");
            item.setPostedDate(LocalDate.now());
            item.setUsername("UserA");
            itemList.add(item);
        }

        for(int i = 0; i < 5 ; i++){
            Item item = new Item();
            item.setStatus("Inactive");
            item.setPostedDate(LocalDate.now());
            item.setUsername("UserB");
            inactiveItemList.add(item);
        }

        User user = new User();

        image.setName("DefaultImage");
        imageDbRepository.save(image);
    }

    @Test
    void getItems() {
        when(itemRepository.findByStatusEqualsOrderByPostedDateDescTitleAsc("Active")).thenReturn(itemList);
        ItemsList newItemList = itemsService.getAllItems();
        assertThat(newItemList).isNotNull();
        assertThat(newItemList.isEmpty()).isFalse();
    }

    @Test
    void getItemsReturnsEmptyArrayList() {
        List<Item> emptyList = new ArrayList<>();
        when(itemRepository.findByStatusEqualsOrderByPostedDateDescTitleAsc("Active")).thenReturn(emptyList);
        itemRepository.deleteAll();
        ItemsList itemsList = itemsService.getAllItems();
        assertThat(itemsList.isEmpty()).isTrue();
    }

    @Test
    void getItemById() {
       Item item = itemList.get(0);
       item.setId(1L);
       when(itemRepository.findById(anyLong())).thenReturn(Optional.of(item));
       Item foundItem = itemsService.getItemById(item.getId());
       assertThat(foundItem).isNotNull();
       assertThat(item.getId()).isEqualTo(foundItem.getId());
    }

    @Test
    void getItemByIdReturnsNoContent() {
        when(itemRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatExceptionOfType(ItemNotFoundException.class).isThrownBy(() ->
                itemsService.getItemById(12345L));
    }

    @Test
    void getActiveItemsByUsernameReturnsList() {
        when(itemRepository.findByStatusEqualsAndUsernameEqualsOrderByPostedDateDescTitleAsc("Active", "UserA")).thenReturn(itemList);
        ItemsList activeItemsList = itemsService.getActiveItemsByUsername("UserA");
        assertThat(activeItemsList).isNotNull();
        assertThat(activeItemsList.isEmpty()).isFalse();
    }

    @Test
    void getInactiveItemsByUsernameReturnsList() {
        when(itemRepository.findByStatusEqualsAndUsernameEqualsOrderByPostedDateDescTitleAsc("Inactive", "UserB")).thenReturn(inactiveItemList);
        ItemsList activeItemsList = itemsService.getInactiveItemsByUsername("UserB");
        assertThat(activeItemsList).isNotNull();
        assertThat(activeItemsList.isEmpty()).isFalse();
    }

    @Test
    void getItemsOneActiveOneInactive() {
        Item item1 = new Item();
        item1.setExpirationDate(LocalDate.now());
        item1.setStatus("Active");
        itemList.add(item1);
        itemRepository.save(item1);

        List<Item> updatedItemList = itemList;
        updatedItemList.remove(item1);

        when(itemRepository.findByStatusEqualsOrderByPostedDateDescTitleAsc("Active")).thenReturn(itemList).thenReturn(updatedItemList);

        ItemsList activeItemsList = itemsService.getAllItems();

        assertThat(activeItemsList.getLength()).isEqualTo(updatedItemList.size());
    }

    @Test
    void addItem() {

        Item item = itemList.get(0);
        item.setTitle("Cool item");
        item.setCategory("Furniture");
        item.setPrice(10);
        item.setContactMethod("Call");
        item.setWillingToShip(true);
        item.setPaymentMethod("Venmo");
        item.setCondition("Good");
        item.setDescription("New item in great condition");
        item.setEmailAddress("email@email.com");
        item.setPhoneNumber("123-456-7890");
        item.setLocation("Bloomington");
        item.setStatus("Active");

        UpdateItem updateItem = new UpdateItem();
        updateItem.setTitle("Cool item");
        updateItem.setCategory("Furniture");
        updateItem.setPrice(10);
        updateItem.setContactMethod("Call");
        updateItem.setWillingToShip(true);
        updateItem.setPaymentMethod("Venmo");
        updateItem.setCondition("Good");
        updateItem.setDescription("New item in great condition");
        updateItem.setEmailAddress("email@email.com");
        updateItem.setPhoneNumber("123-456-7890");
        updateItem.setLocation("Bloomington");
        updateItem.setStatus("Active");

        when(itemRepository.save(any(Item.class))).thenReturn(item);
        Item returnedItem = itemsService.addItem(updateItem, user);
        assertThat(returnedItem).isNotNull();
        assertThat(returnedItem.getTitle()).isEqualTo(updateItem.getTitle());
        assertThat(image.getId()).isEqualTo(returnedItem.getPicture());
    }

    @Test
    void addItemBadRequest() {
        UpdateItem updateItem = new UpdateItem();
        assertThatExceptionOfType(InvalidItemException.class).isThrownBy(() ->
                itemsService.addItem(updateItem, user));
    }

    @Test
    void updateItem(){
        Item item = itemList.get(0);
        item.setTitle("Cool item");
        item.setCategory("Furniture");
        item.setPrice(10);
        item.setContactMethod("Call");
        item.setWillingToShip(true);
        item.setPaymentMethod("Venmo");
        item.setCondition("Good");
        item.setDescription("New item in great condition");
        item.setEmailAddress("email@email.com");
        item.setPhoneNumber("123-456-7890");
        item.setLocation("Bloomington");
        item.setStatus("Active");

        Item item2 = new Item();
        item2.setTitle("Another amazing thing!");
        item2.setCategory("Furniture");
        item2.setPrice(10);
        item2.setContactMethod("Call");
        item2.setWillingToShip(true);
        item2.setPaymentMethod("Venmo");
        item2.setCondition("Good");
        item2.setDescription("New item in great condition");
        item2.setEmailAddress("email@email.com");
        item2.setPhoneNumber("123-456-7890");
        item2.setLocation("Bloomington");
        item2.setStatus("Active");

        UpdateItem updateItem = new UpdateItem();
        updateItem.setTitle("Another amazing thing!");

        when(itemRepository.findByTitle(item.getTitle())).thenReturn(Optional.of(item));
        when(itemRepository.save(any(Item.class))).thenReturn(item2);
        Item returnedItem = itemsService.updateItemByTitle(item.getTitle(), updateItem);
        assertThat(returnedItem).isNotNull();
        assertThat(returnedItem.getTitle()).isEqualTo(item2.getTitle());
    }

    @Test
    void updateItemNoItemFound() {
        when(itemRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatExceptionOfType(ItemNotFoundException.class).isThrownBy(() ->
                itemsService.updateItem(anyLong(), new UpdateItem(), user));
    }

    @Test
    void updateItemBadRequest() {
        when(itemRepository.findById(anyLong())).thenThrow(InvalidItemException.class);
        assertThatExceptionOfType(InvalidItemException.class).isThrownBy(() ->
                itemsService.updateItem(anyLong(), new UpdateItem(), user));
    }

    @Test
    void updateViews(){
        Item item = itemList.get(0);
        item.setTitle("Washing Machine");
        item.setViews(0);
        item.setId(1L);

        Item item2 = new Item();
        item2.setTitle("Washing Machine");
        item2.setViews(1);


        UpdateItem updateItem = new UpdateItem();
        updateItem.setViews(1);

        when(itemRepository.findById(anyLong())).thenReturn(Optional.of(item));
        when(itemRepository.save(any(Item.class))).thenReturn(item2);
        Item returnedItem = itemsService.updateViews(item.getId());
        assertThat(returnedItem).isNotNull();
        assertThat(returnedItem.getViews()).isEqualTo(item2.getViews());
    }

    @Test
    void updateViolationFlagged(){
        Item item = itemList.get(0);
        item.setTitle("Washing Machine");
        item.setViolationFlagged(true);
        item.setId(1L);

        Item item2 = new Item();
        item2.setTitle("Washing Machine");
        item2.setViolationFlagged(true);


        UpdateItem updateItem = new UpdateItem();
        updateItem.setViolationFlagged(true);

        when(itemRepository.findById(anyLong())).thenReturn(Optional.of(item));
        when(itemRepository.save(any(Item.class))).thenReturn(item2);
        Item returnedItem = itemsService.updateViolationFlagged(item.getId(), updateItem);
        assertThat(returnedItem).isNotNull();
        assertThat(returnedItem.isViolationFlagged()).isEqualTo(item2.isViolationFlagged());
    }

    @Test
    void deleteItem() {
        Item item = itemList.get(0);
        item.setId(1L);
        item.setUsername("blah");
        when(itemRepository.findById(anyLong())).thenReturn(Optional.of(item));
        itemsService.deleteItem(item.getId(), user);
        verify(itemRepository).delete(any(Item.class));
    }

    @Test
    void deleteItemNoItemFound() {
        when(itemRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatExceptionOfType(ItemNotFoundException.class).isThrownBy(() ->
                itemsService.deleteItem(12345L, user));
    }

    @Test
    void testUsingStrictRegex() {
        String emailAddress = "username@domain.com";
        assertTrue(itemsService.patternMatches(emailAddress));
    }

    @Test
    void testingPatternMatchesWithName() {
        String emailAddress = "brendan";
        assertFalse(itemsService.patternMatches(emailAddress));
    }

    @Test
    public void whenMatchesTenDigitsNumberParenthesis_thenCorrect() {
        String phoneNumber = "(202) 555-0125";
        assertTrue(itemsService.isValidPhoneNumber(phoneNumber));
    }

    @Test
    void isNotValidPhoneNumber() {
        String phoneNumber = "asdf";
        assertFalse(itemsService.isValidPhoneNumber(phoneNumber));
    }

    @Test
    void addItemNotAuthorized() {
        assertThatExceptionOfType(UserNotAuthorizedException.class).isThrownBy(() ->
                itemsService.addItem(new UpdateItem(), new User()));
    }

    @Test
    void updateItemNotAuthorized() {
        when(itemRepository.findById(anyLong())).thenThrow(UserNotAuthorizedException.class);
        assertThatExceptionOfType(UserNotAuthorizedException.class).isThrownBy(() ->
                itemsService.updateItem(anyLong(), new UpdateItem(), user));
    }

    @Test
    void deleteItemNotAuthorized() {
        Item item = itemList.get(0);
        itemRepository.save(item);
        when(itemRepository.findById(anyLong())).thenReturn(Optional.of(item));
        assertThatExceptionOfType(UserNotAuthorizedException.class).isThrownBy(() ->
                itemsService.deleteItem(anyLong(), user));
    }
}
