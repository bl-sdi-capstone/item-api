package com.statefarm.itemapi.Entities;

import javax.persistence.*;

@Entity
@Table(name = "contact_method")
public class ContactMethod {
    @Id
    private String name;

    public ContactMethod() {
    }

    public ContactMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
