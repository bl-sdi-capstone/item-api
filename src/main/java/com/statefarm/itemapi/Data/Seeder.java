package com.statefarm.itemapi.Data;

import com.google.common.reflect.ClassPath;
import com.statefarm.itemapi.Entities.*;
import com.statefarm.itemapi.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class Seeder {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ConditionRepository conditionRepository;
    @Autowired
    ContactMethodRepository contactMethodRepository;
    ItemRepository itemRepository;
    @Autowired
    LocationRepository locationRepository;
    @Autowired
    PaymentMethodRepository paymentMethodRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    ImageDbRepository imageDbRepository;

    public Seeder(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public void seed() throws Exception {

        List<Category> categoryList = new ArrayList<>();
        categoryList.add(new Category("Appliances"));
        categoryList.add(new Category("Automotive"));
        categoryList.add(new Category("Children's Items"));
        categoryList.add(new Category("Clothing"));
        categoryList.add(new Category("Electronics"));
        categoryList.add(new Category("Furniture"));
        categoryList.add(new Category("Other"));
        categoryList.add(new Category("Outdoors"));
        categoryList.add(new Category("Pet Supplies"));
        categoryList.add(new Category("Real Estate"));
        categoryList.add(new Category("Sports"));
        categoryList.add(new Category("Tools"));
        categoryRepository.saveAll(categoryList);

        List<Condition> conditionList = new ArrayList<>();
        conditionList.add(new Condition("Like New"));
        conditionList.add(new Condition("Good"));
        conditionList.add(new Condition("Fair"));
        conditionList.add(new Condition("Poor"));
        conditionRepository.saveAll(conditionList);

        List<ContactMethod> contactMethodList = new ArrayList<>();
        contactMethodList.add(new ContactMethod("Email"));
        contactMethodList.add(new ContactMethod("Phone Call"));
        contactMethodList.add(new ContactMethod("Text Message"));
        contactMethodRepository.saveAll(contactMethodList);

        List<Location> locationList = new ArrayList<>();
        locationList.add(new Location("Atlanta"));
        locationList.add(new Location("Bloomington"));
        locationList.add(new Location("Dallas"));
        locationList.add(new Location("Phoenix"));
        locationList.add(new Location("Remote"));
        locationRepository.saveAll(locationList);

        List<PaymentMethod> paymentMethodList = new ArrayList<>();
        paymentMethodList.add(new PaymentMethod("Cash"));
        paymentMethodList.add(new PaymentMethod("Check"));
        paymentMethodList.add(new PaymentMethod("PayPal"));
        paymentMethodList.add(new PaymentMethod("Venmo"));
        paymentMethodList.add(new PaymentMethod("Zelle"));
        paymentMethodList.add(new PaymentMethod("Google Pay"));
        paymentMethodList.add(new PaymentMethod("Apple Pay"));
        paymentMethodRepository.saveAll(paymentMethodList);

        List<Status> statusList = new ArrayList<>();
        statusList.add(new Status("Active"));
        statusList.add(new Status("Inactive"));
        statusRepository.saveAll(statusList);


        if (imageDbRepository.findAll().isEmpty()) {
            Image image = new Image();
            image.setName("DefaultImage");
            BufferedImage bufferedImage = ImageIO.read(new ClassPathResource("defaultImage.jpg").getInputStream());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", byteArrayOutputStream);
            byte[] data = byteArrayOutputStream.toByteArray();
            image.setContent(data);
            imageDbRepository.save(image);
        }
    }
}
